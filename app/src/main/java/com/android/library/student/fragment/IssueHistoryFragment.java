package com.android.library.student.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.library.student.R;
import com.android.library.student.activity.DashboardActivity;
import com.android.library.student.adapter.IssueBookRecyclerAdapter;
import com.android.library.student.javaClass.Constants;
import com.android.library.student.javaClass.LocalValues;
import com.android.library.student.model.IssuedBook;
import com.android.library.student.model.IssuedBookList;
import com.android.library.student.model.IssuedBookListResponse;
import com.android.library.student.webService.WebAPIInterface;
import com.android.library.student.webService.WebAPIService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class IssueHistoryFragment extends Fragment {

    private RecyclerView issueBookRecycler;
    private ArrayList<IssuedBookList> issueBookList;
    private IssueBookRecyclerAdapter issueBookRecyclerAdapter;

    public IssueHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        issueBookList = new ArrayList<>();
        issueBookRecyclerAdapter = new IssueBookRecyclerAdapter(getContext(), issueBookList);

        WebAPIService
                .getRetrofitClient()
                .create(WebAPIInterface.class)
                .issuedBookList(LocalValues.getLongPreferences(Constants.USER_UID),
                        LocalValues.getStringPreferences(Constants.LOGIN_TYPE))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<IssuedBookListResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        System.out.println("API subscribed");
                    }

                    @Override
                    public void onNext(IssuedBookListResponse issuedBookListResponse) {
                        System.out.println(issuedBookListResponse.getCode());
                        if (issuedBookListResponse.getCode() == 200) {
                            issueBookList.clear();
                            issueBookList.addAll(issuedBookListResponse.getData());
                            issueBookRecyclerAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        System.out.println("API Completed.");
                    }
                });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_issue_history, container, false);
        assert getContext() != null;
        ((DashboardActivity) getContext()).getNavigationView().setCheckedItem(R.id.issue_history);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        issueBookRecycler = rootView.findViewById(R.id.issued_book_list);
        issueBookRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        issueBookRecycler.setAdapter(issueBookRecyclerAdapter);
    }

}
