package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by chhatrasal on 8/2/18.
 */

/*{
   "_id":1413401,
   "name":"student 1",
   "contactNo":9541400001,
   "email":"studen1@gmail.com",
   "batch":2014,
   "branch":"CSE",
   "issuedBookNos":1,
   "issuedBooks": [{
      "bookId":500001,
      "book":"Book 1",
      "author":"Author 1",
      "issueDate":"12/12/2017",
      "returnDate":"12/02/2018",
      "fine":0
   }],
   "issueLimit":3
}*/

public class Profile {

    @SerializedName("_id")
    private long uid;
    @SerializedName("name")
    private String name;
    @SerializedName("contactNo")
    private long contactNo;
    @SerializedName("email")
    private String email;
    @SerializedName("batch")
    private int batch;
    @SerializedName("branch")
    private String branch;
    @SerializedName("issuedBookNos")
    private int issuedBookNos;
    @SerializedName("issuedBooks")
    private ArrayList<IssuedBookList> issuedBooks;
    @SerializedName("issueLimit")
    private int issueLimit;
    @SerializedName("password")
    private String password;
    @SerializedName("imageUrl")
    private String imageUrl;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getContactNo() {
        return contactNo;
    }

    public void setContactNo(long contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getBatch() {
        return batch;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public int getIssuedBookNos() {
        return issuedBookNos;
    }

    public void setIssuedBookNos(int issuedBookNos) {
        this.issuedBookNos = issuedBookNos;
    }

    public ArrayList<IssuedBookList> getIssuedBooks() {
        return issuedBooks;
    }

    public void setIssuedBooks(ArrayList<IssuedBookList> issuedBooks) {
        this.issuedBooks = issuedBooks;
    }

    public int getIssueLimit() {
        return issueLimit;
    }

    public void setIssueLimit(int issueLimit) {
        this.issueLimit = issueLimit;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    //    @Override
//    public String toString() {
//        return "{ Profile : { 'rollNo' : " + rollNo +
//                ", 'name' : '" + name +
//                "', 'fatherName' : '" + fatherName +
//                "', 'contactNo' : " + contactNo +
//                ", 'email' : '" + email +
//                "', 'branch' : '" + branch +
//                "', 'batch' : " + batch +
//                ", 'issuedBooksNos' : " + issuedBookNos +
//                ". 'issueLimit' : " + issueLimit +
//                ", 'issuedBooks' : [" + issuedBooks.toString() + " } } ";
//    }

}
