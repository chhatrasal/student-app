package com.android.library.student.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.android.library.student.R;
import com.android.library.student.activity.DashboardActivity;
import com.android.library.student.adapter.DashBoardGridRecyclerAdapter;
import com.android.library.student.adapter.DashBoardGridViewAdapter;
import com.android.library.student.model.DashBoardViewModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashBoardFragment extends Fragment {

    private ArrayList<DashBoardViewModel> list;

    public DashBoardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_dash_board, container, false);
        assert getContext() != null;
        ((DashboardActivity) getContext()).getNavigationView().setCheckedItem(R.id.dashboard);
        RecyclerView gridRecyclerView = rootView.findViewById(R.id.dashboard_recycler_view);
        gridRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        list = new ArrayList<>();
        list.add(new DashBoardViewModel("Issue Book", R.drawable.issue_book200px));
        list.add(new DashBoardViewModel("Return Book", R.drawable.return_book200px));
        list.add(new DashBoardViewModel("Profile", R.drawable.update_profile200px));
        list.add(new DashBoardViewModel("Book List", R.drawable.update_book200px));
        list.add(new DashBoardViewModel("Issued History", R.drawable.home200px));
        gridRecyclerView.setAdapter(new DashBoardGridRecyclerAdapter(getContext(), list));
        return rootView;
    }

}
