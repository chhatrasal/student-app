package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Chhatrasal on 02-04-2018.
 */

public class IssueCartList {

    @SerializedName("bookId")
    private long bookId;
    @SerializedName("name")
    private String bookName;
    @SerializedName("author")
    private String author;

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
