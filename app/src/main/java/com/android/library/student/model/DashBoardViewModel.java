package com.android.library.student.model;

import android.graphics.drawable.Drawable;

/**
 * Created by Chhatrasal on 22-01-2018.
 */

public class DashBoardViewModel {
    private String optionName;
    private int imageId;

    public DashBoardViewModel(String optionName, int imageId) {
        this.optionName = optionName;
        this.imageId = imageId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
