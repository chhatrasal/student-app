package com.android.library.student.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.library.student.R;
import com.android.library.student.activity.DashboardActivity;
import com.android.library.student.adapter.CartListRecyclerAdapter;
import com.android.library.student.javaClass.Constants;
import com.android.library.student.javaClass.LocalValues;
import com.android.library.student.model.IssueCartList;
import com.android.library.student.model.IssueCartListResponse;
import com.android.library.student.webService.WebAPIInterface;
import com.android.library.student.webService.WebAPIService;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment {

    private RecyclerView cartListRecyclerView;
    private CartListRecyclerAdapter cartListIssueAdapter, cartListReturnAdapter;
    private ArrayList<IssueCartList> cartListIssueBookList, cartListReturnBookList;
    private RadioButton issueBookRadioButton;
    private RadioButton returnBookRadioButton;
    private RadioGroup radioGroup;

    public CartFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getActivity() != null;
        ((DashboardActivity) getActivity()).getSupportActionBar().setTitle("Cart");
        cartListIssueBookList = new ArrayList<>();
        cartListReturnBookList = new ArrayList<>();
        cartListReturnAdapter = new CartListRecyclerAdapter(getContext(), cartListReturnBookList);
        cartListIssueAdapter = new CartListRecyclerAdapter(getContext(), cartListIssueBookList);

        WebAPIService
                .getRetrofitClient()
                .create(WebAPIInterface.class)
                .issueCartList(LocalValues.getLongPreferences(Constants.USER_UID),
                        LocalValues.getStringPreferences(Constants.LOGIN_TYPE))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<IssueCartListResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.v("####", "API Subscribed");
                    }

                    @Override
                    public void onNext(IssueCartListResponse issueCartListResponse) {
                        if (issueCartListResponse.getCode() == 200) {
                            cartListIssueBookList.clear();
                            cartListIssueBookList.addAll(issueCartListResponse.getData());
                            cartListIssueAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Log.v("####", "@ + " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.v("####", "API Completed.");
                    }
                });

        WebAPIService
                .getRetrofitClient()
                .create(WebAPIInterface.class)
                .returnCartList(LocalValues.getLongPreferences(Constants.USER_UID),
                        LocalValues.getStringPreferences(Constants.LOGIN_TYPE))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<IssueCartListResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.v("####", "API Subscribed");
                    }

                    @Override
                    public void onNext(IssueCartListResponse issueCartListResponse) {
                        cartListReturnBookList.clear();
                        if (issueCartListResponse.getCode() == 200) {
                            cartListReturnBookList.addAll(issueCartListResponse.getData());
                        }
                        cartListReturnAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Log.v("####", "@ + " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.v("####", "API Completed.");
                    }
                });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_cart, container, false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View rootView) {
        cartListRecyclerView = rootView.findViewById(R.id.cart_list_recycler_view);
        cartListRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        radioGroup = rootView.findViewById(R.id.book_state_radio_group);
        issueBookRadioButton = rootView.findViewById(R.id.issue_book_radio_button);
        returnBookRadioButton = rootView.findViewById(R.id.return_book_radio_button);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.issue_book_radio_button) {
                    LocalValues.setStringPreferences("collectionName", "issueCart");
                    returnBookRadioButton.setChecked(false);
                } else if (checkedId == R.id.return_book_radio_button) {
                    LocalValues.setStringPreferences("collectionName", "returnCart");
                    issueBookRadioButton.setChecked(false);
                }
            }
        });
        if (issueBookRadioButton.isChecked()) {
            LocalValues.setStringPreferences("collectionName", "issueCart");
            cartListRecyclerView.setAdapter(cartListIssueAdapter);
        } else {
            LocalValues.setStringPreferences("collectionName", "returnCart");
            cartListRecyclerView.setAdapter(cartListReturnAdapter);
        }

        issueBookRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    returnBookRadioButton.setChecked(false);
                    cartListRecyclerView.swapAdapter(cartListIssueAdapter, true);
                    cartListIssueAdapter.notifyDataSetChanged();
                }
            }
        });
        returnBookRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    issueBookRadioButton.setChecked(false);
                    cartListRecyclerView.swapAdapter(cartListReturnAdapter, true);
                    cartListReturnAdapter.notifyDataSetChanged();
                }
            }
        });
    }

}
