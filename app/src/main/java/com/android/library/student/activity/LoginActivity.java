package com.android.library.student.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.library.student.R;
import com.android.library.student.javaClass.Constants;
import com.android.library.student.javaClass.LocalValues;
import com.android.library.student.model.IssuedBookList;
import com.android.library.student.model.LoginResponse;
import com.android.library.student.webService.WebAPIInterface;
import com.android.library.student.webService.WebAPIService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity {

    private EditText userNameEditText, passwordEditText;
    private RadioButton studentRadioButton, facultyRadioButton;
    private Button togglePasswordButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalValues.setLocalSharedPreference(LoginActivity.this);
        if (LocalValues.getBooleanPreferences(Constants.IS_LOGGED_IN)) {
            startActivity(new Intent(LoginActivity.this, DashboardActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
            finish();
        } else {
            setContentView(R.layout.activity_login);

            initViews();

            onClickEvents();
        }
    }

    private View.OnClickListener onTogglePasswordViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (passwordEditText.getTransformationMethod() == PasswordTransformationMethod.getInstance()) {
                //password is visible
                passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else if(passwordEditText.getTransformationMethod() == HideReturnsTransformationMethod.getInstance()) {
                //password is hidden
                passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    };
    private View.OnClickListener onSignInClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!TextUtils.isEmpty(userNameEditText.getText()) &&
                    !TextUtils.isEmpty(passwordEditText.getText()) &&
                    (facultyRadioButton.isChecked() || studentRadioButton.isChecked())) {

                // Login Credentials for POST request.
                Map<String, String> loginCredentials = new HashMap<>();
                loginCredentials.put(Constants.USERNAME, userNameEditText.getText().toString());
                loginCredentials.put(Constants.USER_PASSWORD, passwordEditText.getText().toString());
                loginCredentials.put(Constants.LOGIN_TYPE,
                        studentRadioButton.isChecked() ?
                                studentRadioButton.getText().toString().toLowerCase() :
                                facultyRadioButton.getText().toString().toLowerCase());
                Log.v("######", " " + loginCredentials.toString());
                WebAPIService
                        .getRetrofitClient()
                        .create(WebAPIInterface.class)
                        .login(loginCredentials)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<LoginResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                Log.v("Login API", "# Inside Subscribe");
                            }

                            @Override
                            public void onNext(LoginResponse loginResponse) {
                                try {
                                    if (loginResponse.getCode() == 200) {
                                        Log.v("######", "" + loginResponse);
                                        LocalValues.setLongPreferences(Constants.USER_UID, loginResponse.getLoginData().getUid());
                                        LocalValues.setIntegerPreferences(Constants.USER_BATCH, loginResponse.getLoginData().getBatch());
                                        LocalValues.setIntegerPreferences(Constants.USER_ISSUE_LIMIT, loginResponse.getLoginData().getIssueLimit());
                                        LocalValues.setIntegerPreferences(Constants.USER_ISSUED_BOOK_NOS, loginResponse.getLoginData().getIssuedBookNos());
                                        LocalValues.setLongPreferences(Constants.USER_CONTACT_NUMBER, loginResponse.getLoginData().getContactNo());
                                        LocalValues.setStringPreferences(Constants.USER_NAME, loginResponse.getLoginData().getName());
                                        LocalValues.setStringPreferences(Constants.USER_EMAIL, loginResponse.getLoginData().getEmail());
                                        LocalValues.setStringPreferences(Constants.USER_BRANCH, loginResponse.getLoginData().getBranch());
                                        LocalValues.setBooleanPreferences(Constants.IS_LOGGED_IN, true);
                                        LocalValues.setStringPreferences(Constants.USER_PASSWORD, loginResponse.getLoginData().getPassword());
                                        LocalValues.setStringPreferences(Constants.USER_PROFILE_IMAGE_URL, loginResponse.getLoginData().getImageUrl());

                                        String bookIdsString = getStringFromArrayList(loginResponse.getLoginData().getIssuedBooks());
                                        LocalValues.setStringPreferences(Constants.USER_ISSUED_BOOKS, bookIdsString);
                                        LocalValues.setStringPreferences(Constants.LOGIN_TYPE, studentRadioButton.isChecked() ?
                                                studentRadioButton.getText().toString().toLowerCase() :
                                                facultyRadioButton.getText().toString().toLowerCase());
                                        startActivity(new Intent(LoginActivity.this, DashboardActivity.class)
                                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                        finish();
                                    } else {
                                        Toast.makeText(LoginActivity.this, "" + loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.v("####Login API", "# " + e.getMessage());
                                    Toast.makeText(LoginActivity.this, "please enter correct credentials", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.v("Login API", "# " + e.getMessage());
                                Toast.makeText(LoginActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onComplete() {
                                Log.v("Login API", "Login API successful.");
                            }
                        });
            } else {
                String message = "Please fill ";
                if (TextUtils.isEmpty(userNameEditText.getText())) {
                    if (message.equalsIgnoreCase("Please fill ")) {
                        message = message + "username";
                    } else {
                        message = message + ",username";
                    }
                }
                if (TextUtils.isEmpty(passwordEditText.getText())) {
                    if (message.equalsIgnoreCase("Please fill ")) {
                        message = message + "password";
                    } else {
                        message = message + ",password";
                    }
                }
                message = message + ".";
                Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private String getStringFromArrayList(ArrayList<IssuedBookList> issuedBooks) {
        String result;
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<IssuedBookList>>() {
        }.getType();
        result = gson.toJson(issuedBooks, type);
        System.out.println(result);
        return result;
    }

    private void initViews() {
        userNameEditText = findViewById(R.id.text_user_name);
        passwordEditText = findViewById(R.id.text_password);
        studentRadioButton = findViewById(R.id.radio_student);
        facultyRadioButton = findViewById(R.id.radio_faculty);
        togglePasswordButton = findViewById(R.id.toggle_password_button);
    }

    private void onClickEvents() {
        togglePasswordButton.setOnClickListener(onTogglePasswordViewClickListener);
        findViewById(R.id.button_sign_in).setOnClickListener(onSignInClickListener);
    }
}
