package com.android.library.student.javaClass;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by chhatrasal on 8/2/18.
 */

public class LocalValues {

    private static SharedPreferences localPreference;

    public static void setLocalSharedPreference(Activity activity) {
        localPreference = activity.getPreferences(Context.MODE_PRIVATE);
    }

    public static SharedPreferences getLocalPreference() {
        return localPreference;
    }

    public static String getStringPreferences(String key) {
        return localPreference.getString(key, "");
    }

    public static void setStringPreferences(String key, String value){
        localPreference
                .edit()
                .putString(key,value)
                .apply();
    }

    public static int getIntegerPreferences(String key) {
        return localPreference.getInt(key, 0);
    }

    public static void setIntegerPreferences(String key, int value){
        localPreference
                .edit()
                .putInt(key,value)
                .apply();
    }
    public static boolean getBooleanPreferences(String key) {
        return localPreference.getBoolean(key, false);
    }

    public static void setBooleanPreferences(String key, boolean value){
        localPreference
                .edit()
                .putBoolean(key,value)
                .apply();
    }

    public static long getLongPreferences(String key) {
        return localPreference.getLong(key, 0);
    }

    public static void setLongPreferences(String key, long value){
        localPreference
                .edit()
                .putLong(key,value)
                .apply();
    }
}
