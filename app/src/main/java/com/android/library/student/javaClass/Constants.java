package com.android.library.student.javaClass;

/**
 * Created by Chhatrasal on 25-01-2018.
 */

public class Constants {

    public static final String SCREEN_WIDTH = "screenWidth";
    public static final String SCREEN_HEIGHT = "screenHeight";

//    public static final String BASE_URL = "http://192.168.44.68:4000/app/android/";
    public static final String BASE_URL = "http://192.168.1.6:4000/app/android/";
//    public static final String BASE_URL = "http://192.168.245.56:4000/app/android/";
    public static final String IS_LOGGED_IN = "isLoggedIn";

    public static final String USERNAME = "userName";
    public static final String LOGIN_TYPE = "loginType";

    public static final String _ID = "_id";
    public static final String USER_UID = "uid";
    public static final String USER_NAME = "studentName";
    public static final String USER_CONTACT_NUMBER = "contactNo";
    public static final String USER_EMAIL = "email";
    public static final String USER_BATCH = "batch";
    public static final String USER_BRANCH = "branch";
    public static final String USER_ISSUED_BOOK_NOS = "issueBookNos";
    public static final String USER_ISSUE_LIMIT = "issueLimit";
    public static final String USER_ISSUED_BOOKS = "issuedBooks";
    public static final String USER_PASSWORD = "password";
    public static final String USER_PROFILE_IMAGE_URL = "imageUrl";
}
