package com.android.library.student.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.library.student.model.DashBoardViewModel;

import java.util.ArrayList;

/**
 * Created by Chhatrasal on 22-01-2018.
 */

public class DashBoardGridViewAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<DashBoardViewModel> list;

    public DashBoardGridViewAdapter(Context context, ArrayList<DashBoardViewModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(context);
        imageView.setMaxHeight(200);
        LinearLayout.LayoutParams imageViewLayoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        imageView.setLayoutParams(imageViewLayoutParams);
        imageView.setImageResource(list.get(position).getImageId());
        return imageView;
    }
}
