package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chhatrasal on 8/2/18.
 */

public class IssuedBookList {

    @SerializedName("bookId")
    private long bookId;
    @SerializedName("issueDate")
    private long issuedDate;
    @SerializedName("isbn")
    private long isbn;
    @SerializedName("bookName")
    private String bookName;
    @SerializedName("author")
    private String author;
    @SerializedName("returnDate")
    private long returnDate;
    @SerializedName("fine")
    private long fine;

    public long getBookId() {
        return bookId;
    }

    public void setBookId(long bookId) {
        this.bookId = bookId;
    }

    public long getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(long issuedDate) {
        this.issuedDate = issuedDate;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public long getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(long returnDate) {
        this.returnDate = returnDate;
    }

    public long getFine() {
        return fine;
    }

    public void setFine(long fine) {
        this.fine = fine;
    }

    //    @Override
//    public String toString() {
//        return "IssuedBookList : { 'bookId' : " + bookId +
//                ", 'book' : '" + bookName +
//                "', 'author' : '" + authorName +
//                "', 'issueDate' : '" + issueDate +
//                "', 'returnDate' : '" + returnDate +
//                "', 'fine' : " + fine + " } } ";
//    }
}

