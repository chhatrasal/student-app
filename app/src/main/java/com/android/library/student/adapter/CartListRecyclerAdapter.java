package com.android.library.student.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.library.student.R;
import com.android.library.student.javaClass.Constants;
import com.android.library.student.javaClass.LocalValues;
import com.android.library.student.model.IssueCartList;
import com.android.library.student.model.RemoveCartBookResponse;
import com.android.library.student.webService.WebAPIInterface;
import com.android.library.student.webService.WebAPIService;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

/**
 * Created by Chhatrasal on 02-04-2018.
 */

public class CartListRecyclerAdapter extends RecyclerView.Adapter<CartListRecyclerAdapter.ViewHolder> {

    private Context context;
    private ArrayList<IssueCartList> list;
    private String cartType;

    public CartListRecyclerAdapter(Context context, ArrayList<IssueCartList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        return new ViewHolder(inflater.inflate(R.layout.cart_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getBookIdTextView().setText(String.valueOf(list.get(position).getBookId()));
        holder.getBookNameTextView().setText(list.get(position).getBookName());
        holder.getAuthorTextView().setText(list.get(position).getAuthor());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView bookNameTextView;
        private TextView authorTextView;
        private TextView bookIdTextView;
        private ProgressBar progressBar;

        private View.OnLongClickListener itemLongClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new AlertDialog.Builder(context)
                        .setMessage("Remove book")
                        .setCancelable(false)
                        .setMessage("Do you want to remove this book from cart?")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                progressBar.setVisibility(View.VISIBLE);
                                Map<String, Object> hashMap = new HashMap<>();
                                hashMap.put("bookId", bookIdTextView.getText().toString());
                                hashMap.put(Constants.USER_UID, LocalValues.getLongPreferences(Constants.USER_UID));
                                hashMap.put("collectionName", LocalValues.getStringPreferences("collectionName"));
                                WebAPIService
                                        .getRetrofitClient()
                                        .create(WebAPIInterface.class)
                                        .removeCartBook(hashMap)
                                        .subscribeOn(Schedulers.computation())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new Observer<ResponseBody>() {
                                            @Override
                                            public void onSubscribe(Disposable d) {
                                                Log.v("#####", "on subscribe.");
                                            }

                                            @Override
                                            public void onNext(ResponseBody removeCartBookResponse) {
                                                try {
                                                    JSONObject jsonObject = new JSONObject(removeCartBookResponse.string());
                                                    if (jsonObject.getInt("code") == 200) {
                                                        if (jsonObject.getJSONObject("data").getJSONObject("lastErrorObject").getInt("n") == 1) {
                                                            list.remove(getAdapterPosition());
                                                            progressBar.setVisibility(View.GONE);
                                                            notifyDataSetChanged();
                                                            dialog.dismiss();

                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                Log.v("######API Error", "" + e.getMessage());
                                                dialog.dismiss();
                                                progressBar.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onComplete() {
                                                Log.v("#####", "on process completed.");
                                            }
                                        });
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create()
                        .show();
                return true;
            }
        };

        public ViewHolder(View itemView) {
            super(itemView);
            bookIdTextView = itemView.findViewById(R.id.book_id_text_view);
            bookNameTextView = itemView.findViewById(R.id.book_name_text_view);
            authorTextView = itemView.findViewById(R.id.author_text_view);

            progressBar = new ProgressBar(context);
            progressBar.setIndeterminate(true);
            itemView.setOnLongClickListener(itemLongClickListener);
        }

        private TextView getBookNameTextView() {
            return bookNameTextView;
        }

        private TextView getAuthorTextView() {
            return authorTextView;
        }

        private TextView getBookIdTextView() {
            return bookIdTextView;
        }
    }
}
