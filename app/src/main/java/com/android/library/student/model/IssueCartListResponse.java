package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Chhatrasal on 02-04-2018.
 */

public class IssueCartListResponse {

    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ArrayList<IssueCartList> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<IssueCartList> getData() {
        return data;
    }

    public void setData(ArrayList<IssueCartList> data) {
        this.data = data;
    }
}
