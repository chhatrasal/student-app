package com.android.library.student.model;

/**
 * Created by Chhatrasal on 25-01-2018.
 */

public class IssuedBook {

    private String bookName;
    private String issueDate;
    private String returnDate;

    public IssuedBook() {
    }

    public IssuedBook(String bookName, String issueDate, String returnDate) {
        this.bookName = bookName;
        this.issueDate = issueDate;
        this.returnDate = returnDate;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }
}
