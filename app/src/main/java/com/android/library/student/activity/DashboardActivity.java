package com.android.library.student.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android.library.student.R;
import com.android.library.student.fragment.BookListFragment;
import com.android.library.student.fragment.CartFragment;
import com.android.library.student.fragment.DashBoardFragment;
import com.android.library.student.fragment.IssueBookFragment;
import com.android.library.student.fragment.IssueHistoryFragment;
import com.android.library.student.fragment.ProfileFragment;
import com.android.library.student.fragment.ReturnBookFragment;
import com.android.library.student.javaClass.Constants;
import com.android.library.student.javaClass.LocalValues;

public class DashboardActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private SharedPreferences localPreference;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        localPreference = getPreferences(MODE_PRIVATE);
        getScreenResolution();

        addNavigationView(savedInstanceState);
    }

    private NavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener =
            new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    if (item.isChecked()) {
                        item.setChecked(false);
                    } else {
                        item.setChecked(true);
                    }
                    drawerLayout.closeDrawers();
                    assert getSupportActionBar() != null;
                    getSupportActionBar().setTitle(item.getTitle());
                    return displayFragments(item.getItemId());
                }
            };

    public SharedPreferences getLocalPreference() {
        return localPreference;
    }

    public NavigationView getNavigationView() {
        return navigationView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cart_list:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new CartFragment())
                        .addToBackStack("Cart")
                        .commit();
                return true;
            case R.id.log_out:
                LocalValues.getLocalPreference().edit().clear().apply();
                LocalValues.setBooleanPreferences(Constants.IS_LOGGED_IN, false);
                startActivity(new Intent(DashboardActivity.this, LoginActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;

    }

    private void addNavigationView(Bundle savedInstanceState) {
        navigationView = findViewById(R.id.navigation);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer);
        navigationView.setNavigationItemSelectedListener(onNavigationItemSelectedListener);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        if (savedInstanceState == null) {
            assert getSupportActionBar() != null;
            getSupportActionBar().setTitle(R.string.dashboard);
            displayFragments(R.id.dashboard);
        }
    }

    private boolean displayFragments(int itemId) {
        switch (itemId) {
            case R.id.dashboard:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new DashBoardFragment())
                        .commit();
                return true;
            case R.id.issue_book:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new IssueBookFragment())
                        .commit();
                return true;
            case R.id.return_book:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new ReturnBookFragment())
                        .commit();
                return true;
            case R.id.profile_view:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new ProfileFragment())
                        .commit();
                return true;
            case R.id.book_list:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new BookListFragment())
                        .commit();
                return true;
            case R.id.issue_history:
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new IssueHistoryFragment())
                        .commit();
                return true;
        }
        return false;
    }

    private void getScreenResolution() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        localPreference
                .edit()
                .putInt(Constants.SCREEN_WIDTH, width)
                .putInt(Constants.SCREEN_HEIGHT, height)
                .apply();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof IssueBookFragment) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new DashBoardFragment())
                    .commit();
        } else if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof ReturnBookFragment) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new DashBoardFragment())
                    .commit();
        } else if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof BookListFragment) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new DashBoardFragment())
                    .commit();
        } else if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof ProfileFragment) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new DashBoardFragment())
                    .commit();
        } else if (getSupportFragmentManager().findFragmentById(R.id.container) instanceof IssueHistoryFragment) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, new DashBoardFragment())
                    .commit();
        } else {
            super.onBackPressed();
        }
    }
}
