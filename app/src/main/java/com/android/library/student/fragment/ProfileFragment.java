package com.android.library.student.fragment;


import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.library.student.R;
import com.android.library.student.activity.DashboardActivity;
import com.android.library.student.javaClass.Constants;
import com.android.library.student.javaClass.LocalValues;
import com.android.library.student.model.UpdateProfileResponse;
import com.android.library.student.webService.WebAPIInterface;
import com.android.library.student.webService.WebAPIService;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private static final int SELECT_PICTURE = 100;

    private EditText name;
    private EditText uid;
    private EditText batch;
    private EditText branch;
    private EditText contactNo;
    private EditText eMail;
    private EditText password;
    private EditText oldPassword;
    private EditText newPassword;
    private Button update;
    private ImageView profileImage;
    private LinearLayout passwordLayout;
    private Picasso.Builder picassoBuilder;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        assert getContext() != null;
        ((DashboardActivity) getContext()).getNavigationView().setCheckedItem(R.id.profile_view);
        if (rootView.getId() == R.id.fragment_profile) {
            initView(rootView);

            updateFields();

            onClickEvents();
        }
        return rootView;
    }

    private void initView(View rootView) {
        name = rootView.findViewById(R.id.full_name_text);
        name.setTag(name.getKeyListener());
        name.setKeyListener(null);
        uid = rootView.findViewById(R.id.roll_no_text);
        uid.setTag(uid.getKeyListener());
        uid.setKeyListener(null);
        batch = rootView.findViewById(R.id.batch_text);
        batch.setTag(batch.getKeyListener());
        batch.setKeyListener(null);
        branch = rootView.findViewById(R.id.branch_text);
        branch.setTag(branch.getKeyListener());
        branch.setKeyListener(null);
        contactNo = rootView.findViewById(R.id.contact_no_text);
        contactNo.setTag(contactNo.getKeyListener());
        contactNo.setKeyListener(null);
        eMail = rootView.findViewById(R.id.email_text);
        eMail.setTag(eMail.getKeyListener());
        eMail.setKeyListener(null);
        update = rootView.findViewById(R.id.submit_button);
        password = rootView.findViewById(R.id.password_text);
        password.setTag(password.getKeyListener());
        password.setKeyListener(null);
        oldPassword = rootView.findViewById(R.id.old_password_text);
        newPassword = rootView.findViewById(R.id.new_password_text);
        passwordLayout = rootView.findViewById(R.id.update_password_layout);

        profileImage = rootView.findViewById(R.id.profile_image);
        assert getContext() != null;
        picassoBuilder = new Picasso.Builder(getContext());
        picassoBuilder.loggingEnabled(true).indicatorsEnabled(true);
    }

    private void updateFields() {
        name.setText(LocalValues.getStringPreferences(Constants.USER_NAME));
        uid.setText(String.valueOf(LocalValues.getLongPreferences(Constants.USER_UID)));
        batch.setText(String.valueOf(LocalValues.getIntegerPreferences(Constants.USER_BATCH)));
        branch.setText(LocalValues.getStringPreferences(Constants.USER_BRANCH).toUpperCase());
        contactNo.setText(String.valueOf(LocalValues.getLongPreferences(Constants.USER_CONTACT_NUMBER)));
        eMail.setText(LocalValues.getStringPreferences(Constants.USER_EMAIL));
        password.setText(LocalValues.getStringPreferences(Constants.USER_PASSWORD));
        Log.v("######", "" + Constants.BASE_URL + LocalValues.getStringPreferences(Constants.USER_PROFILE_IMAGE_URL));
        picassoBuilder.build()
                .load(Constants.BASE_URL + LocalValues.getStringPreferences(Constants.USER_PROFILE_IMAGE_URL))
                .resize(130, 130)
                .centerCrop()
                .into(profileImage);
    }

    private void onClickEvents() {
        contactNo.setOnClickListener(onContactNoClickListener);
        eMail.setOnClickListener(onEMailClickListener);
        update.setOnClickListener(onUpdateClickListener);
        password.setOnClickListener(onPasswordClickListener);
        profileImage.setOnClickListener(profileImageClickListener);
    }

    private View.OnClickListener onContactNoClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            contactNo.setKeyListener((KeyListener) contactNo.getTag());
            contactNo.setSelection(contactNo.getText().length());
        }
    };
    private View.OnClickListener onEMailClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            eMail.setKeyListener((KeyListener) eMail.getTag());
            eMail.setSelection(eMail.getText().length());
        }
    };
    private View.OnClickListener onPasswordClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            password.setKeyListener((KeyListener) password.getTag());
            password.setSelection(password.getText().length());
            passwordLayout.setVisibility(View.VISIBLE);
        }
    };
    private View.OnClickListener onUpdateClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (String.valueOf(password.getText()).equals(String.valueOf(oldPassword.getText()))) {
                final HashMap<String, String> updateData = new HashMap<>();
                updateData.put(Constants._ID, uid.getText().toString());
                updateData.put(Constants.USER_EMAIL, eMail.getText().toString());
                updateData.put(Constants.USER_CONTACT_NUMBER, contactNo.getText().toString());
//            updateData.put(Constants.PASSWORD,newPassword.getText().toString());

                String loginType = LocalValues.getStringPreferences(Constants.LOGIN_TYPE);
                WebAPIService
                        .getRetrofitClient()
                        .create(WebAPIInterface.class)
                        .profile(loginType, updateData)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<UpdateProfileResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                Log.v("profile API", "API Subscribed");
                            }

                            @Override
                            public void onNext(UpdateProfileResponse updateProfileResponse) {
                                if (updateProfileResponse.getCode() == 200) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(updateProfileResponse.getData());
                                        if (jsonObject.getInt("nModified") == 1) {
                                            LocalValues.setStringPreferences(Constants.USER_EMAIL, eMail.getText().toString());
                                            LocalValues.setLongPreferences(Constants.USER_CONTACT_NUMBER, Long.valueOf(contactNo.getText().toString()));
                                            Toast.makeText(getContext(), "Successful...", Toast.LENGTH_SHORT).show();
                                        }
//                                LocalValues.setStringPreferences(Constants.PASSWORD, newPassword.getText().toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.v("profile API", "# " + e.getMessage());
                            }

                            @Override
                            public void onComplete() {
                                Log.v("profile API", "API execution completed");
                            }
                        });
            } else {
                Toast.makeText(getContext(), "Please enter correct old password.", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener profileImageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivityForResult(new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI),
                    SELECT_PICTURE);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == SELECT_PICTURE && data != null) {
            final Uri selectedImage = data.getData();
            if (selectedImage != null) {
                final File originalFile = new File(getFileFromUri(selectedImage));
                final String newFileName = getNewFileName(originalFile);
                assert getContext() != null;
                assert getContext().getContentResolver() != null;
                Log.v("######", "" + getContext().getContentResolver().getType(selectedImage));
                RequestBody imagePart = RequestBody.create(MediaType.parse(getContext().getContentResolver().getType(selectedImage)), originalFile);
                MultipartBody.Part file = MultipartBody.Part.createFormData("image", newFileName, imagePart);

                RequestBody idPart = RequestBody.create(MultipartBody.FORM, String.valueOf(LocalValues.getLongPreferences(Constants.USER_UID)));
                RequestBody loginTypePart = RequestBody.create(MultipartBody.FORM, LocalValues.getStringPreferences(Constants.LOGIN_TYPE));

                WebAPIService
                        .getRetrofitClient()
                        .create(WebAPIInterface.class)
                        .uploadImage(idPart, loginTypePart, file)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<ResponseBody>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                Log.v("####", "Upload API subscribed.");
                            }

                            @Override
                            public void onNext(ResponseBody responseBody) {
                                try {
                                    JSONObject responseObject = new JSONObject(responseBody.string());
                                    Log.v("######", "# " + responseObject.toString());
                                    if (responseObject.getInt("code") == 200) {
                                        picassoBuilder.build()
                                                .load(selectedImage)
                                                .resize(130, 130)
                                                .centerCrop()
                                                .into(profileImage);
                                        LocalValues.setStringPreferences(Constants.USER_PROFILE_IMAGE_URL,responseObject.getString("data"));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.v("######", "#" + e.getMessage());
                            }

                            @Override
                            public void onComplete() {
                                Log.v("####", "Upload API completed.");
                            }
                        });
            }
        }
    }

    private String getFileFromUri(Uri uri) {
//        StringBuilder result = new StringBuilder();
        String result;
        assert getContext() != null;
        Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
        if (cursor == null) {
//            result.append(uri.getPath());
            result = uri.getPath();
            assert cursor != null;
            cursor.close();
            return result;
        }
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
//        result.append(uri.getPath());
        result = cursor.getString(idx);
        cursor.close();
        return result;
    }

    private String getNewFileName(File file) {
        String oldFileName = file.getName();
        Log.v("#####", "" + oldFileName);
        return "" + LocalValues.getLongPreferences(Constants.USER_UID)
                + "." + oldFileName.substring(oldFileName.lastIndexOf(".") + 1);
    }
}
