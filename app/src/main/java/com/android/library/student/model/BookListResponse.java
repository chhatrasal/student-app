package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by chhatrasal on 9/2/18.
 */

public class BookListResponse {

    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ArrayList<Book> bookList;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Book> getBooksList() {
        return bookList;
    }

    public void setBooksList(ArrayList<Book> bookList) {
        this.bookList = bookList;
    }

//    @Override
//    public String toString() {
//        return "{ LoginResponse : { 'code' : " + code +
//                ", 'message' : '" + message +
//                "', 'loginData ' : " + loginData +
//                "} }";
//    }
}
