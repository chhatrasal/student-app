package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Chhatrasal on 24-01-2018.
 */

public class Book {

    @SerializedName("_id")
    private long isbn;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("available")
    private int available;
    @SerializedName("bookName")
    private String bookName;
    @SerializedName("author")
    private String bookAuthor;
    @SerializedName("tags")
    private ArrayList<String> tags;
    @SerializedName("bookIds")
    private ArrayList<Long> bookIds;

    public Book() {
    }

    public Book(long isbn, int quantity, int available, String bookName, String bookAuthor,
                ArrayList<String> tags, ArrayList<Long> bookIds) {
        this.isbn = isbn;
        this.quantity = quantity;
        this.available = available;
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
        this.tags = tags;
        this.bookIds = bookIds;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public ArrayList<Long> getBookIds() {
        return bookIds;
    }

    public void setBookIds(ArrayList<Long> bookIds) {
        this.bookIds = bookIds;
    }
}
