package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Chhatrasal on 01-04-2018.
 */

public class IssuedBookListResponse {

    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private ArrayList<IssuedBookList> data;

    public IssuedBookListResponse() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<IssuedBookList> getData() {
        return data;
    }

    public void setData(ArrayList<IssuedBookList> data) {
        this.data = data;
    }
}
