package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chhatrasal on 8/2/18.
 */

/*{
   "code":200,
   "message":"Success",
   "loginData":{
     "_id":1413401,
     "name":"student 1",
     "fatherName":"father 1",
     "contactNo":9541400001,
     "email":"studen1@gmail.com",
     "password":"password1",
     "batch":2014,
     "branch":"CSE",
     "issuedBookNos":1,
     "issuedBooks":[
       {
          "bookId":500001,
          "book":"Book 1",
          "author":"Author 1",
          "issueDate":"12/12/2017",
          "returnDate":"12/02/2018",
          "fine":0
        }],
     "issueLimit":3
   }
}*/

public class LoginResponse {

    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Profile loginData;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Profile getLoginData() {
        return loginData;
    }

    public void setLoginData(Profile loginData) {
        this.loginData = loginData;
    }

    @Override
    public String toString() {
        return "{ LoginResponse : { 'code' : " + code +
                ", 'message' : '" + message +
                "', 'loginData ' : " + loginData +
                "} }";
    }
}
