package com.android.library.student.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.library.student.R;
import com.android.library.student.activity.DashboardActivity;
import com.android.library.student.fragment.BookListFragment;
import com.android.library.student.fragment.IssueBookFragment;
import com.android.library.student.fragment.IssueHistoryFragment;
import com.android.library.student.fragment.ProfileFragment;
import com.android.library.student.fragment.ReturnBookFragment;
import com.android.library.student.model.DashBoardViewModel;

import java.util.ArrayList;

/**
 * Created by Chhatrasal on 04-04-2018.
 */

public class DashBoardGridRecyclerAdapter extends RecyclerView.Adapter<DashBoardGridRecyclerAdapter.ViewHolder> {

    private Context context;
    private ArrayList<DashBoardViewModel> list;

    public DashBoardGridRecyclerAdapter(Context context, ArrayList<DashBoardViewModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        return new ViewHolder(inflater.inflate(R.layout.dashboard_grid_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getImageTextVuew().setText(list.get(position).getOptionName());
        holder.getImageView().setImageResource(list.get(position).getImageId());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private TextView imageTextVuew;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view);
            imageTextVuew = itemView.findViewById(R.id.image_text_view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imageTextVuew.getText().toString().equalsIgnoreCase("Issue Book")) {
                        ((DashboardActivity) context)
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, new IssueBookFragment())
                                .commit();
                    } else if (imageTextVuew.getText().toString().equalsIgnoreCase("Return Book")) {
                        ((DashboardActivity) context)
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, new ReturnBookFragment())
                                .commit();
                    } else if (imageTextVuew.getText().toString().equalsIgnoreCase("Profile")) {
                        ((DashboardActivity) context)
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, new ProfileFragment())
                                .commit();
                    } else if (imageTextVuew.getText().toString().equalsIgnoreCase("Book List")) {
                        ((DashboardActivity) context)
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, new BookListFragment())
                                .commit();
                    } else if (imageTextVuew.getText().toString().equalsIgnoreCase("Issued History")) {
                        ((DashboardActivity) context)
                                .getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.container, new IssueHistoryFragment())
                                .commit();
                    }
                }
            });
        }

        public ImageView getImageView() {
            return imageView;
        }

        public TextView getImageTextVuew() {
            return imageTextVuew;
        }
    }
}
