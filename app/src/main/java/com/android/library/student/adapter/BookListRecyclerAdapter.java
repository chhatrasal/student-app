package com.android.library.student.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.library.student.R;
import com.android.library.student.model.Book;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Chhatrasal on 24-01-2018.
 */

public class BookListRecyclerAdapter extends RecyclerView.Adapter<BookListRecyclerAdapter.ViewHolder> {

    private ArrayList<Book> originalList;
    private ArrayList<Book> searchList;
    private Context context;

    public BookListRecyclerAdapter(ArrayList<Book> originalList, Context context) {
        this.originalList = originalList;
        this.context = context;
        searchList = new ArrayList<>(originalList);
    }


    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        return new ViewHolder(inflater.inflate(R.layout.book_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.getBookName().setText(searchList.get(position).getBookName());
        holder.getAuthorName().setText(searchList.get(position).getBookAuthor());
        holder.getAvailableBooks().setText(String.valueOf(searchList.get(position).getAvailable()));
        holder.getTotalBooks().setText(String.valueOf(searchList.get(position).getQuantity()));
        holder.getItemRootLayout().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "" + searchList.get(holder.getAdapterPosition()).getBookName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView bookName;
        private TextView authorName;
        private TextView totalBooks;
        private TextView availableBooks;
        private LinearLayout itemRootLayout;

        ViewHolder(View rootView) {
            super(rootView);
            bookName = rootView.findViewById(R.id.book_name);
            authorName = rootView.findViewById(R.id.author_name);
            totalBooks = rootView.findViewById(R.id.total_quantity);
            availableBooks = rootView.findViewById(R.id.available_quantity);
            itemRootLayout = rootView.findViewById(R.id.book_list_layout);
        }

        private TextView getBookName() {
            return bookName;
        }

        private TextView getAuthorName() {
            return authorName;
        }

        private TextView getTotalBooks() {
            return totalBooks;
        }

        private TextView getAvailableBooks() {
            return availableBooks;
        }

        private LinearLayout getItemRootLayout() {
            return itemRootLayout;
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                ArrayList<Book> filteredList = new ArrayList<>();
                if (originalList == null) {
                    originalList = new ArrayList<>(searchList);

                }
                if (constraint == null || constraint.length() == 0) {
                    filterResults.count = originalList.size();
                    filterResults.values = originalList;
                } else {
                    for (int index = 0; index < originalList.size(); ++index) {
                        try {
                            Book book = originalList.get(index);
//                            boolean result = tags.contains(constraint.toString().toUpperCase());
//                            Log.v("######", "# " + result);
                            if (book.getBookName().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                                    book.getBookAuthor().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                                    String.valueOf(book.getIsbn()).toLowerCase().contains(constraint.toString().toLowerCase())) {
                                filteredList.add(book);
                            } else {
                                for (String tag : book.getTags()) {
                                    if (tag.contains(constraint.toString().toLowerCase())) {
                                        filteredList.add(book);
                                    }
                                }
                                if (!filteredList.contains(book)) {
                                    for (long bookId : book.getBookIds()) {
                                        if (String.valueOf(bookId).toLowerCase().contains(constraint.toString().toLowerCase()) && !filteredList.contains(book)) {
                                            filteredList.add(book);
                                        }
                                    }
                                }
                            }
                            if (filteredList == null) {
                                filteredList = new ArrayList<>();
                            }
                            filterResults.values = filteredList;
                            filterResults.count = filteredList.size();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }
                return filterResults;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                searchList = (ArrayList<Book>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}
