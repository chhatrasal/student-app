package com.android.library.student.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.library.student.R;
import com.android.library.student.activity.DashboardActivity;
import com.android.library.student.adapter.BookListRecyclerAdapter;
import com.android.library.student.model.Book;
import com.android.library.student.model.BookListResponse;
import com.android.library.student.webService.WebAPIInterface;
import com.android.library.student.webService.WebAPIService;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookListFragment extends Fragment {

    private RecyclerView bookAvailabilityList;
    private EditText searchText;
    private ArrayList<Book> bookArrayList;
    private BookListRecyclerAdapter listViewAdapter;

    public BookListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        WebAPIService
                .getRetrofitClient()
                .create(WebAPIInterface.class)
                .allBookList()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BookListResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.v("AllBookList API", "# Inside Subscribe");
                    }

                    @Override
                    public void onNext(BookListResponse bookListResponse) {
                        System.out.println(" Inside the next method");
                        if (bookListResponse.getCode() == 200) {

                            bookAvailabilityList.setLayoutManager(new LinearLayoutManager(getContext()));
                            bookArrayList = bookListResponse.getBooksList();
                            listViewAdapter = new BookListRecyclerAdapter(bookArrayList, getContext());
                            bookAvailabilityList.setAdapter(listViewAdapter);
                        } else {
                            bookAvailabilityList.setLayoutManager(new LinearLayoutManager(getContext()));
                            bookArrayList = new ArrayList<>();
                            listViewAdapter = new BookListRecyclerAdapter(bookArrayList, getContext());
                            bookAvailabilityList.setAdapter(listViewAdapter);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.v("AllBookList API", "# " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {
                        Log.v("AllBookList API", "Login API successful.");
                    }
                });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_book_list, container, false);
        assert getContext() != null;
        ((DashboardActivity) getContext()).getNavigationView().setCheckedItem(R.id.book_list);
        initViews(view);

        searchText.addTextChangedListener(onBookInputSearch);
        return view;
    }

    private TextWatcher onBookInputSearch = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            listViewAdapter.getFilter().filter(s);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void initViews(View view) {
        bookAvailabilityList = view.findViewById(R.id.book_list);
        searchText = view.findViewById(R.id.search_input);
    }
}
