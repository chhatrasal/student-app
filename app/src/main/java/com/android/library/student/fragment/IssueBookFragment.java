package com.android.library.student.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.library.student.R;
import com.android.library.student.activity.DashboardActivity;
import com.android.library.student.javaClass.Constants;
import com.android.library.student.javaClass.LocalValues;
import com.android.library.student.webService.WebAPIInterface;
import com.android.library.student.webService.WebAPIService;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class IssueBookFragment extends Fragment {

    private static final int CAMERA_PERMISSION_CONSTANT = 100;
    private BarcodeDetector barcodeDetector;
    private SurfaceView barcodeCameraView;
    private EditText bookIdInput;
    private CameraSource cameraSource;
    private ArrayList<Map<String, Object>> outputList;

    public IssueBookFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        barcodeDetector = new BarcodeDetector.Builder(getContext())
                .setBarcodeFormats(Barcode.QR_CODE | Barcode.ISBN)
                .build();
        outputList = new ArrayList<>();
        if (!barcodeDetector.isOperational()) {
            Toast.makeText(getContext(), "Could not set up barcode detector.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_issue_book, container, false);
        assert getContext() != null;
        ((DashboardActivity) getContext()).getNavigationView().setCheckedItem(R.id.issue_book);
        barcodeCameraView = rootView.findViewById(R.id.barcode_camera_view);
        barcodeCameraView.setLayoutParams(new LinearLayout.LayoutParams(
                getContext().getResources().getDisplayMetrics().widthPixels*4/5,
                getContext().getResources().getDisplayMetrics().widthPixels*4/5));
        bookIdInput = rootView.findViewById(R.id.book_id_edit_text);
        rootView.findViewById(R.id.submit_button).setOnClickListener(onSubmitButtonClickListener);
        assert getContext() != null;
        cameraSource = new CameraSource.Builder(getContext(), barcodeDetector)
                .setRequestedPreviewSize(((DashboardActivity) getContext())
                                .getLocalPreference()
                                .getInt(Constants.SCREEN_WIDTH, 480),
                        ((DashboardActivity) getContext())
                                .getLocalPreference()
                                .getInt(Constants.SCREEN_HEIGHT, 640))
                .setAutoFocusEnabled(true)
                .setFacing(CameraSource.CAMERA_FACING_BACK)
                .build();

        barcodeCameraView.getHolder().addCallback(cameraSurfaceHolderCallback);
        barcodeDetector.setProcessor(onBarCodeDetected);
        return rootView;
    }

    private SurfaceHolder.Callback cameraSurfaceHolderCallback = new SurfaceHolder.Callback() {
        @SuppressLint("MissingPermission")
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CONSTANT);
                cameraSource.start(barcodeCameraView.getHolder());
            } catch (Exception e) {
                e.printStackTrace();
                Log.v("#####surface Created", "@ " + e.getMessage());
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            cameraSource.stop();
        }
    };

    private Detector.Processor<Barcode> onBarCodeDetected = new Detector.Processor<Barcode>() {
        @Override
        public void release() {

        }

        @Override
        public void receiveDetections(Detector.Detections<Barcode> detections) {
            try {
                SparseArray barCodes = detections.getDetectedItems();
                if (barCodes.size() != 0) {
                    for (int index = 0; index < barCodes.size(); ++index) {
                        Barcode barcode = (Barcode) barCodes.valueAt(index);
                        JSONObject jsonObject = new JSONObject(barcode.displayValue);
                        final String isbnValue = "" + jsonObject.getString("bookId");
                        assert getContext() != null;
                        ((DashboardActivity) getContext()).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bookIdInput.setText(isbnValue);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                assert getContext() != null;
                ((DashboardActivity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bookIdInput.setText(null);
                    }
                });
                e.printStackTrace();
            }
        }
    };

    private View.OnClickListener onSubmitButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!bookIdInput.getText().toString().isEmpty()) {
                Map<String, Object> hashMap = new HashMap<>();
                hashMap.put(Constants._ID, Long.valueOf(bookIdInput.getText().toString()));
                hashMap.put(Constants.USER_UID, LocalValues.getLongPreferences(Constants.USER_UID));
                hashMap.put(Constants.LOGIN_TYPE, LocalValues.getStringPreferences(Constants.LOGIN_TYPE));
                outputList.add(hashMap);

                WebAPIService
                        .getRetrofitClient()
                        .create(WebAPIInterface.class)
                        .issueBook(outputList)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<ResponseBody>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                System.out.println("# Inside Subscribe");
                            }

                            @Override
                            public void onNext(ResponseBody responseBody) {
                                try {
                                    JSONObject jsonObject = new JSONObject(responseBody.string());
                                    if (jsonObject.getInt("code") == 200) {
                                        assert getFragmentManager() != null;
                                        getFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.container, new CartFragment())
                                                .commit();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                System.out.println(e.getMessage());
                            }

                            @Override
                            public void onComplete() {
                                System.out.println("Appi request is completed.");
                            }
                        });
            } else {
                Toast.makeText(getContext(), "Please fill the ISBN or scan a QR code.", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 || grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    cameraSource.start(barcodeCameraView.getHolder());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            quitActivity();
        }
    }

    private void quitActivity() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
