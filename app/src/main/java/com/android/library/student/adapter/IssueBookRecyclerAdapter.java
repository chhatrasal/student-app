package com.android.library.student.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.library.student.R;
import com.android.library.student.model.IssuedBook;
import com.android.library.student.model.IssuedBookList;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Chhatrasal on 25-01-2018.
 */

public class IssueBookRecyclerAdapter extends RecyclerView.Adapter<IssueBookRecyclerAdapter.ViewHolder> {

    private Context context;
    private ArrayList<IssuedBookList> list;

    public IssueBookRecyclerAdapter(Context context, ArrayList<IssuedBookList> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        return new ViewHolder(inflater.inflate(R.layout.issue_book_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.getBookNameTextView().setText(list.get(position).getBookName());
        holder.getIssueDateTextView().setText(getStringDate(list.get(position).getIssuedDate()));
        if(list.get(position).getReturnDate() != 0) {
            holder.getReturnDateTextView().setText(getStringDate(list.get(position).getReturnDate()));
        }else {
            holder.getReturnDateTextView().setText("Issued");
        }
        System.out.println(new Date().getTime());
        holder.getIssuedBookLayout().setOnClickListener(new OnItemClickListener(position));
    }

    private String getStringDate(long longDate) {
        String stringDate = null;
        try {
            stringDate = new SimpleDateFormat("dd/MM/yy", Locale.UK).format(new Date(longDate));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringDate;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class OnItemClickListener implements View.OnClickListener {

        private int position;

        public OnItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Toast.makeText(context, "" + list.get(position).getBookName(), Toast.LENGTH_SHORT).show();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView bookNameTextView;
        private TextView issueDateTextView;
        private TextView returnDateTextView;
        private LinearLayout issuedBookLayout;

        public ViewHolder(View itemView) {
            super(itemView);

            bookNameTextView = itemView.findViewById(R.id.book_name);
            issueDateTextView = itemView.findViewById(R.id.issue_date);
            returnDateTextView = itemView.findViewById(R.id.return_date);
            issuedBookLayout = itemView.findViewById(R.id.issued_book_layout);
        }

        private TextView getBookNameTextView() {
            return bookNameTextView;
        }

        private TextView getIssueDateTextView() {
            return issueDateTextView;
        }

        private TextView getReturnDateTextView() {
            return returnDateTextView;
        }

        private LinearLayout getIssuedBookLayout() {
            return issuedBookLayout;
        }
    }
}
