package com.android.library.student.webService;

import com.android.library.student.model.BookListResponse;
import com.android.library.student.model.IssueCartList;
import com.android.library.student.model.IssueCartListResponse;
import com.android.library.student.model.IssuedBookListResponse;
import com.android.library.student.model.LoginResponse;
import com.android.library.student.model.RemoveCartBookResponse;
import com.android.library.student.model.UpdateProfileResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by chhatrasal on 8/2/18.
 */

public interface WebAPIInterface {

    @POST("login")
    Observable<LoginResponse> login(@Body Map<String, String> loginCredentials);

    @GET("bookList")
    Observable<BookListResponse> allBookList();

    @POST("profile")
    Observable<UpdateProfileResponse> profile(@Query("loginType") String loginType, @Body Map<String, String> updateData);

    @POST("issueBook")
    Observable<ResponseBody> issueBook(@Body ArrayList<Map<String, Object>> bookList);

    @POST("returnBook")
    Observable<ResponseBody> returnBook(@Body ArrayList<Map<String, Object>> bookList);

    @GET("issuedBookList")
    Observable<IssuedBookListResponse> issuedBookList(@Query("uid") long uid, @Query("loginType") String loginType);

    @GET("issueCartList")
    Observable<IssueCartListResponse> issueCartList(@Query("uid") long uid, @Query("loginType") String loginType);

    @GET("returnCartList")
    Observable<IssueCartListResponse> returnCartList(@Query("uid") long uid, @Query("loginType") String loginType);

    @POST("removeCartBook")
    Observable<ResponseBody> removeCartBook(@Body Map<String, Object> deleteData);

    @Multipart
    @POST("uploadImage")
    Observable<ResponseBody> uploadImage(@Part("id") RequestBody id,
                                         @Part("loginType") RequestBody loginType,
                                         @Part MultipartBody.Part image);
}
