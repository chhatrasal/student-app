package com.android.library.student.model;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

/**
 * Created by Chhatrasal on 04-04-2018.
 */

public class RemoveCartBookResponse {
    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private JSONObject data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
